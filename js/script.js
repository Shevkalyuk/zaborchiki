$(document).ready(function() {
    /* Modal */
    function showModal(modalId, closeOnClick) {
        var modal = $('#' + modalId);
        var close = modal.find('.modal-close');
        modal.fadeIn(200);

        close.click(function() {
            modal.fadeOut(200);
        });

        if (closeOnClick) {
            modal.click(function() {
                $(this).fadeOut(200);
            });
        }
    }

    $('.button').not('.button-anchor').click(function(event) {
        event.preventDefault();
        var modal = $(this).attr('data-modal');

        if (modal !== '') {
            showModal(modal, false);
        }
    });

    /* Feedback and Order form validation */
    $('input, textarea').focus(function() {
        if ($(this).hasClass('invalid')) {
            $(this).removeClass('invalid');
        }
    });

    $('#submit-message-form').click(function(event) {
        event.preventDefault();
        var valid_name = checkName();
        var valid_email = checkEmail();
        var valid_message = checkMessage();

        if (valid_name && valid_email && valid_message) {
            submitMessageForm();
        }
    });

    $('#submit-order-form').click(function(event) {
        event.preventDefault();
        var valid_name = checkName();
        var valid_email = checkEmail();
        var valid_message = checkMessage();
        var valid_phone = checkPhone();

        if (valid_name && valid_email && valid_message && valid_phone) {
            submitOrderForm();
        }
    });

    function checkName() {
        var $field = $('#input-name');
        if ('' === $field.val()) {
            $field.addClass('invalid');
            return false;
        }
        return true;
    }

    function checkEmail() {
        var $field = $('#input-email');
        var emailPattern = new RegExp(/^.+?@.+?\..+$/);
        var userEmail = $field.val();
        if ('' === userEmail || !emailPattern.test(userEmail)) {
            $field.addClass('invalid');
            return false;
        }
        return true;
    }

    function checkMessage() {
        var $field = $('#input-message');
        if ('' === $field.val()) {
            $field.addClass('invalid');
            return false;
        }
        return true;
    }

    function checkPhone() {
        var $field = $('#input-phone');
        if ('' === $field.val()) {
            $field.addClass('invalid');
            return false;
        }
        return true;
    }

    function submitMessageForm() {
        var form = $('#feedback-message-form');
        $.ajax({
            type: "POST",
            url: "mail.php",
            data: form.serialize()
        }).done(function() {
            showModal('modal-feedback-ok', true);
            setTimeout(function() {
                form.trigger("reset");
            }, 1000);
        }).fail(function() {
            showModal('modal-feedback-err', true);
            setTimeout(function() {
                form.trigger("reset");
            }, 1000);
        });
        return false;
    }

    function submitOrderForm() {
        var form = $('#order-form');
        $.ajax({
            type: "POST",
            url: "mail.php",
            data: form.serialize()
        }).done(function() {
            showResult(form);
            setTimeout(function() {
                form.trigger("reset");
            }, 1000);
        }).fail(function() {
            showResult(form);
            setTimeout(function() {
                form.trigger("reset");
            }, 1000);
        });
        return false;
    }

    function showResult(form) {
        var resultBlock = form.closest('.modal-content').find('.modal-action-result');
        resultBlock.css('display', 'flex');
        setTimeout(function() {
            resultBlock.hide();
        }, 3000);
    }

    /* Tabs */

    $('.section-tab__control-link').on('click', function(e) {
        e.preventDefault();
        var item = $(this).closest('.section-tab__control');
        var contentItem = $('.section-tab__content-item');
        var itemPosition = item.index();

        contentItem.eq(itemPosition)
            .add(item)
            .addClass('active')
            .siblings()
            .removeClass('active');
    });

    /* Admin panel forms */

    $(document).on('click', '.apanel-submit', function(event) {
        event.preventDefault();
        submitApanelForm($(this).closest('form'));
    });

    function submitApanelForm(form) {
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize()
        }).done(function() {
            showModal('modal-apanel-ok', true);
        }).fail(function() {
            showModal('modal-apanel-err', true);
        });
        return false;
    }
});
