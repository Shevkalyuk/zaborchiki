<?php

require_once 'lib/Config.php';

$method = $_SERVER['REQUEST_METHOD'];
$config = new Config();

//Script Foreach
$c = true;

if ($method === 'POST') {
    try {
        $form_subject = (isset($_POST['order']) ? $config->emailSubjectOrder : $config->emailSubjectFeedback)
            . $config->sitename;

        foreach ($_POST as $key => $value) {
            $message .= "
    		" . (($c = !$c) ? '<tr>':'<tr style="background-color: #f8f8f8;">' ) . "
    			<td style='padding: 10px; border: #e9e9e9 1px solid;'><b>$key</b></td>
    			<td style='padding: 10px; border: #e9e9e9 1px solid;'>" . htmlspecialchars($value) . "</td>
    		</tr>
    		";
        }

        $message = "<table style='width: 100%;'>$message</table>";

        $headers = "MIME-Version: 1.0" . PHP_EOL .
            "Content-Type: text/html; charset=utf-8" . PHP_EOL .
            'From: ' . adopt($config->projectName) . ' <' . $config->siteEmail . '>' . PHP_EOL;

        mail(implode(', ', $config->adminEmails), adopt($form_subject), $message, $headers );
    } catch (Exception $e) {
        file_put_contents('mail.log', date('d.m.Y H:i:s') . ' Mailing error: ' . $e->getMessage() . PHP_EOL, FILE_APPEND);
        header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
        exit;
    }
}

function adopt($text) {
    return '=?UTF-8?B?' . base64_encode($text) . '?=';
}
