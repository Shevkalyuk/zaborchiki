<main class="index flex">
    <div class="logo-block flex ls-2 fs-2r">
        <div class="button bg-y skew-right fcw shadow htext65 center" data-modal="modal-order">
            <div class="skew-left">
                <span class="upper">Заказать</span>
            </div>
        </div>
        <div class="main-menu flex">
            <div class="logo bg-w skew-right shadow center">
                <div class="skew-left">
                    <img src="%site_address%img/logo.png" alt="Логотип NK" width="500">
                </div>
            </div>
            <div class="menu-items flex bg-w skew-right shadow">
                <nav class="skew-left">
                    <ul class="upper center">
                        <li id="item-1" class="skew-right bg-y-hover">
                            <a href="%site_address%about" class="skew-left">О нас</a>
                        </li>
                        <li id="item-2" class="skew-right bg-y-hover">
                            <a href="%site_address%fences" class="skew-left">Заборы</a>
                        </li>
                        <li id="item-3" class="skew-right bg-y-hover">
                            <a href="%site_address%gates" class="skew-left">Ворота</a>
                        </li>
                        <li id="item-4" class="skew-right bg-y-hover">
                            <a href="%site_address%blog" class="skew-left">Блог</a>
                        </li>
                        <li id="item-5" class="skew-right bg-y-hover">
                            <a href="%site_address%contacts" class="skew-left">Контакты</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="phones-block flex">
            <div class="phones bg-y skew-right fcw shadow htext65 center">
                <div class="skew-left">
                    <span>+7 (952) 392-75-50</span>&nbsp;&nbsp;
                    <span>+7 (952) 392-35-50</span>
                </div>
            </div>
        </div>
    </div>
</main>
