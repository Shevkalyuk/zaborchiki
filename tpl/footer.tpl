<footer>
    <div class="warranty flex">
        <div class="warranty-text ls-2">
            Мы даем гарантию 1 год на нашу работу!
        </div>
        <div class="button bg-y skew-right fcw shadow htext65 center fs-2r" data-modal="modal-order">
            <div class="skew-left">
                <span class="upper">Заказать</span>
            </div>
        </div>
    </div>
    <div class="contacts flex fcw">
        <div>
            <a href="%site_address%" class="block">
                <img src="%site_address%img/logo-white.png" alt="Логотип NK" width="300">
            </a>
        </div>
        <div class="center address">
            &copy; 2018 NK - Откатные ворота и заборы <br>
            Санкт-Петербург <br>
            <a href="%vk_link%" target="_blank">Наша группа ВКонтакте</a>
        </div>
        <div class="footer-menu flex">
            <div>
                <span>+7 (952) 392-75-50</span>&nbsp;&nbsp;
                <span>+7 (952) 392-35-50</span>
            </div>
            <div>
                <ul class="flex upper bg-y skew-right fcw">
                    <li><a href="%site_address%about" class="block">О нас</a></li>
                    <li><a href="%site_address%fences" class="block">Заборы</a></li>
                    <li><a href="%site_address%gates" class="block">Ворота</a></li>
                    <li><a href="%site_address%blog" class="block">Блог</a></li>
                    <li><a href="%site_address%contacts" class="block">Контакты</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
