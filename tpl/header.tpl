<header>
    <div class="nav-wrapper bg-w">
    </div>
    <div class="logo-wrapper">
        <div class="logo-img">
            <div class="logo-bg-header bg-w skew-right"></div>
            <a href="%site_address%">
                <img src="%site_address%img/logo.png" alt="Логотип NK" width="300">
            </a>
        </div>
        <div class="logo-phones flex fs-2r ls-2">
            <div class="phones bg-y skew-right fcw shadow htext65 center">
                <div class="skew-left">
                    <span>+7 (952) 392-75-50</span>&nbsp;&nbsp;
                    <span>+7 (952) 392-35-50</span>
                </div>
            </div>
        </div>
    </div>
    <nav>
        <ul class="flex upper">
            <li class="skew-right bg-y-hover"><a href="%site_address%about" class="block skew-left">О нас</a></li>
            <li class="skew-right bg-y-hover"><a href="%site_address%fences" class="block skew-left">Заборы</a></li>
            <li class="skew-right bg-y-hover"><a href="%site_address%gates" class="block skew-left">Ворота</a></li>
            <li class="skew-right bg-y-hover"><a href="%site_address%blog" class="block skew-left">Блог</a></li>
            <li class="skew-right bg-y-hover"><a href="%site_address%contacts" class="block skew-left">Контакты</a></li>
        </ul>
    </nav>
</header>
