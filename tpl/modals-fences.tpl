<div class="modal" id="modal-profnastil-prices">
    <div class="modal-wrapper">
        <div class="modal-content bg-w">
            <span class="modal-close fas fa-times"></span>
            <h3>Цены на забор из профнастила</h3>
            <div class="modal-row">
                <table class="site-table">
                    <thead>
                        <tr>
                            <td>Высота</td>
                            <td>Профнастил с полимерным покрытием, одна сторона *</td>
                            <td>Профнастил с полимерным покрытием, обе стороны</td>
                            <td>Профнастил оцинкованный</td>
                        </tr>
                    </thead>
                    <tbody>
                        %pricesProfnastil%
                    </tbody>
                </table>
            </div>
            <div class="modal-row">
                * Окрашен с одной стороны в цвет, с другой - в серый грунт.
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-shtaketnik-prices">
    <div class="modal-wrapper">
        <div class="modal-content bg-w">
            <span class="modal-close fas fa-times"></span>
            <h3>Цены на металлический штакетник</h3>
            <div class="modal-row">
                <table class="site-table">
                    <thead>
                        <tr>
                            <td>Высота забора</td>
                            <td>Столбы из проф. трубы</td>
                            <td>Поперечные лаги</td>
                            <td>Бутование щебнем, в одностороннем порядке</td>
                            <td>Бутование щебнем, в шахматном порядке</td>
                            <td>Ленточный фундамент</td>
                        </tr>
                    </thead>
                    <tbody>
                        %pricesMetalFence%
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-rabitsa-prices">
    <div class="modal-wrapper">
        <div class="modal-content bg-w">
            <span class="modal-close fas fa-times"></span>
            <h3>Цены на сетку-рабицу</h3>
            <div class="modal-row">
                <table class="site-table">
                    <thead>
                        <tr>
                            <td>Высота забора</td>
                            <td>Стоимость за метр</td>
                        </tr>
                    </thead>
                    <tbody>
                        %pricesRabitsa%
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-gitter-prices">
    <div class="modal-wrapper">
        <div class="modal-content bg-w">
            <span class="modal-close fas fa-times"></span>
            <h3>Цены на сетку Гиттер</h3>
            <div class="modal-row flex-column">
                <h4>С покрытием из ПВХ</h4>
                <table class="site-table">
                    <thead>
                        <tr>
                            <td rowspan="2">Высота</td>
                            <td colspan="3">Толщина сетки</td>
                        </tr>
                        <tr>
                            <td>3 мм</td>
                            <td>4 мм</td>
                            <td>5 мм</td>
                        </tr>
                    </thead>
                    <tbody>
                        %pricesGitterPvc%
                    </tbody>
                </table>
                <h4>Оцинкованная</h4>
                <table class="site-table">
                    <thead>
                        <tr>
                            <td rowspan="2">Высота</td>
                            <td colspan="3">Толщина сетки</td>
                        </tr>
                        <tr>
                            <td>3 мм</td>
                            <td>3,5 мм</td>
                            <td>5 мм</td>
                        </tr>
                    </thead>
                    <tbody>
                        %pricesGitterZinc%
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
