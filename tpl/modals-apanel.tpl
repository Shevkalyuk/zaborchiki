<div class="modal" id="modal-apanel-ok">
    <div class="modal-wrapper">
        <div class="modal-content bg-y fcw">
            <span class="modal-close fas fa-times"></span>
            <div class="modal-row">
                <div class="modal-icon-wrapper">
                    <i class="fas fa-check-circle"></i>
                </div>
                <p class="text-block-right">
                    Данные успешно обновлены!
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-apanel-err">
    <div class="modal-wrapper">
        <div class="modal-content bg-y fcw">
            <span class="modal-close fas fa-times"></span>
            <div class="modal-row">
                <div class="modal-icon-wrapper">
                    <i class="fas fa-exclamation-triangle"></i>
                </div>
                <p class="text-block-right">
                    Произошла ошибка, данные не сохранены.
                </p>
            </div>
        </div>
    </div>
</div>
