<main class="flex flex-column">
    <section class="flex section-first section-vh fs-lh-3">
        <div class="section-block bg-w flex">
            <h1 class="mb-10 large">Мы - команда!</h1>
            <p class="header-subcaption right">И это не просто слова.</p>
            <p class="center">
                Коллектив компании &laquo;НК&raquo; - это команда профессионалов, занимающаяся проектированием,
                производством и установкой откатных ворот и заборов.
            </p>
            <h2 class="large">Как мы работаем?</h2>
            <div class="arrow-next-section">
                <a href="#projecting" class="block empty">Далее</a>
            </div>
        </div>
    </section>
    <section class="flex section-vh fs-lh-3" id="projecting">
        <div class="section-block bg-w flex h-100">
            <h3 class="large header-underlined">Проектирование и расчет</h3>
            <div class="section-header-underline"></div>
            <div class="flex col-2">
                <div class="flex italic flex-center text-block-left">
                    <p>
                        Наш проектный отдел, после предварительных замеров, подготавливает чертежи и схемы
                        готового изделия.
                    </p>
                </div>
                <div>
                    <img src="%site_address%img/tools.jpg" alt="Инструменты">
                </div>
            </div>
            <div class="flex col-2">
                <div>
                    <img src="%site_address%img/business.jpg" alt="Инструменты">
                </div>
                <div class="flex italic flex-center text-block-right">
                    <p>
                        Мы учитываем все пожелания и требования заказчика -
                        просчитываем стоимость с учетом различных материалов
                    </p>
                </div>
            </div>
            <div class="arrow-next-section">
                <a href="#production" class="block empty">Далее</a>
            </div>
        </div>
    </section>
    <section class="flex section-vh fs-lh-3" id="production">
        <div class="section-block bg-w flex h-100">
            <h3 class="large header-underlined">Производство</h3>
            <div class="section-header-underline"></div>
            <p class="italic center">
                Мы реализуем утвержденный заказчиком <br> проект в собственном производственном цехе.
            </p>
            <h4 class="large">Мы гарантируем:</h4>
            <div class="flex col-2 guarantee-items">
                <div class="flex flex-center">
                    <div class="img-circle img-steel"></div>
                    <p class="small">
                        Только качественные материалы напрямую от завода-изготовителя
                        в Санкт-Петербурге и ЛО
                    </p>
                </div>
                <div class="flex flex-center">
                    <div class="img-circle img-came-logo"></div>
                    <p class="small">
                        Высокое качесто готовых изделий и комплектующих,
                        ввиду сотрудничества с официальными дилерами
                    </p>
                </div>
            </div>
            <div class="arrow-next-section">
                <a href="#assembling" class="block empty">Далее</a>
            </div>
        </div>
    </section>
    <section class="flex section-vh fs-lh-3" id="assembling">
        <div class="section-block bg-w flex h-100">
            <h3 class="large header-underlined">Монтаж и установка</h3>
            <div class="section-header-underline"></div>
            <p class="italic center">
                Завершающий этап реализации проекта - это <br> доставка, установка и автоматизация.
            </p>
            <div class="assembling-items flex">
                <div class="item flex">
                    <div class="img-circle img-delivery"></div>
                    <p class="small text-block-right">
                        По окончании производства мы доставим изделие по частям или уже
                        собранную конструкцию - абсолютно бесплатно!
                    </p>
                </div>
                <div class="item flex">
                    <p class="small text-block-left">
                        Мы подготовим фундамент для установки откатных ворот
                        с учетом всех особенностей поверхности и Вашего участка.
                    </p>
                    <div class="img-circle img-foundation"></div>
                </div>
                <div class="item flex">
                    <div class="img-circle img-automatics"></div>
                    <p class="small text-block-right">
                        Последний этап - это настройка и автоматизация всей системы
                        откатных ворот и передача заказчику.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <div class="call">
        <a href="%site_address%contacts" class="block">Заказать звонок</a>
    </div>
</main>
