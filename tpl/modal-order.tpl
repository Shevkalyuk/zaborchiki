<div class="modal" id="modal-order">
    <div class="modal-wrapper">
        <div class="modal-content bg-w">
            <span class="modal-close fas fa-times"></span>
            <h3>Оформление заказа</h3>
            <div class="modal-row">
                Чтобы сделать заказ, пожалуйста, заполните форму ниже, или свяжитесь с нами
                по телефону или электронной почте.
            </div>
            <div class="modal-row flex col-2">
                <div class="flex flex-column space-between h-100">
                    <form class="feedback-form flex flex-column space-between h-100" action="#" method="post" id="order-form">
                        <div class="flex form-field-row col-2">
                            <div class="form-field">
                                <label for="input-name">Имя</label>
                                <input type="text" name="name" id="input-name">
                            </div>
                            <div class="form-field">
                                <label for="input-phone">Телефон</label>
                                <input type="text" name="phone" id="input-phone">
                            </div>
                        </div>
                        <div class="form-field">
                            <label for="input-email">e-mail</label>
                            <input type="email" name="email" id="input-email">
                        </div>
                        <div class="form-field">
                            <label for="input-message">Пожалуйста, опишите интересующий вас товар</label>
                            <textarea name="message" rows="6" cols="80" id="input-message"></textarea>
                        </div>
                        <div class="form-field flex button-right">
                            <div class="skew-right bg-y shadow button button-small upper" id="submit-order-form">
                                Оформить заказ
                            </div>
                        </div>
                        <input type="hidden" name="order" value="order">
                    </form>
                </div>
                <div class="flex flex-column space-between h-100 contact-box-wrapper">
                    <div class="contact-box bg-y flex">
                        <div class="contact-box-icon icon-email"></div>
                        <div class="contact-box-text">
                            vorotazab@yandex.ru
                        </div>
                    </div>
                    <div class="contact-box bg-y flex">
                        <div class="contact-box-icon icon-phone"></div>
                        <div class="contact-box-text">
                            <span>+7 (952) 392-75-50</span><br>
                            <span>+7 (952) 392-35-50</span>
                        </div>
                    </div>
                    <div class="contact-box bg-y flex">
                        <a href="%vk_link%" target="_blank" class="block flex">
                            <div class="contact-box-icon icon-vk"></div>
                            <div class="contact-box-text">
                                Наша группа ВКонтакте
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="modal-action-result bg-w flex flex-column">
                <p>Ваш заказ успешно оформлен!</p>
                <p>В ближайшее время мы свяжемся с вами.</p>
            </div>
        </div>
    </div>
</div>
