<div class="modal" id="modal-gates-prices">
    <div class="modal-wrapper">
        <div class="modal-content bg-w">
            <span class="modal-close fas fa-times"></span>
            <h3>Цены на ворота</h3>
            <div class="modal-row">
                <p>Пожалуйста, уточняйте цены по электронной почте или по телефону.</p>
            </div>
        </div>
    </div>
</div>
