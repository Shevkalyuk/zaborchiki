<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="%meta_desc%">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    %page_meta%
    <link rel="icon" href="%site_address%img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="%site_address%css/normalize.css">
    <link rel="stylesheet" href="%site_address%css/main.css">
    <link rel="stylesheet" href="%site_address%css/media.css">
    <title>%title%</title>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="%site_address%js/script.js"></script>
    <script src="%site_address%js/fontawesome-all.js"></script>
</head>
<body class="%body_class%">
    <div class="overlay"></div>
    <div class="video-contain">
        <video autoplay loop class="bg-video" id="bg-video">
            <source src="%site_address%video/index.mp4" type="video/mp4">
        </video>
    </div>
    <div class="grid"></div>
    <div class="container">
        %header%
        %main_content%
        %footer%
        %modals%
    </div>
</body>
</html>
