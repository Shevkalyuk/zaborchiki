<div class="modal" id="modal-feedback-ok">
    <div class="modal-wrapper">
        <div class="modal-content bg-y fcw">
            <span class="modal-close fas fa-times"></span>
            <div class="modal-row">
                <div class="modal-icon-wrapper">
                    <i class="fas fa-check-circle"></i>
                </div>
                <p class="text-block-right">
                    Ваше сообщение успешно отправлено!<br />В ближайшее время мы свяжемся с вами по электронной почте.
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-feedback-err">
    <div class="modal-wrapper">
        <div class="modal-content bg-y fcw">
            <span class="modal-close fas fa-times"></span>
            <div class="modal-row">
                <div class="modal-icon-wrapper">
                    <i class="fas fa-exclamation-triangle"></i>
                </div>
                <p class="text-block-right">
                    К сожалению, произошла какая-то ошибка и сообщение не отправлено :(
                    <br />Попробуйте связаться с нами по телефону.
                </p>
            </div>
        </div>
    </div>
</div>
