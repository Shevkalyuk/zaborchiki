<main class="flex flex-column">
    <section class="flex apanel-section">
        <div class="section-block bg-w">
            <h1>Цены на заборы</h1>
            <div>
                <h3>Цены на забор из профнастила</h3>
                <form action="%site_address%apanel/update-prices" name="profnastil" id="form-profnastil" class="apanel-form">
                    <table class="site-table">
                        <thead>
                            <tr>
                                <td style="width: 10rem;">Высота</td>
                                <td>Профнастил с полимерным покрытием, одна сторона</td>
                                <td>Профнастил с полимерным покрытием, обе стороны</td>
                                <td>Профнастил оцинкованный</td>
                            </tr>
                        </thead>
                        <tbody>
                            %pricesProfnastil%
                        </tbody>
                    </table>
                    <div class="form-field flex button-right">
                        <div class="skew-right bg-y shadow button button-small upper apanel-submit">
                            Сохранить
                        </div>
                    </div>
                </form>
            </div>
            <div>
                <h3>Цены на забор из металлического штакетника</h3>
                <form action="%site_address%apanel/update-prices" name="metal_fence" id="form-metal-fence" class="apanel-form">
                    <table class="site-table">
                        <thead>
                            <tr>
                                <td style="width: 10rem;">Высота забора</td>
                                <td>Столбы из проф. трубы</td>
                                <td style="width: 10rem;">Поперечные лаги</td>
                                <td>Бутование щебнем, в одностороннем порядке</td>
                                <td>Бутование щебнем, в шахматном порядке</td>
                                <td style="width: 10rem;">Ленточный фундамент</td>
                            </tr>
                        </thead>
                        <tbody>
                            %pricesMetalFence%
                        </tbody>
                    </table>
                    <div class="form-field flex button-right">
                        <div class="skew-right bg-y shadow button button-small upper apanel-submit">
                            Сохранить
                        </div>
                    </div>
                </form>
            </div>
            <div>
                <h3>Цены на сетку-рабицу</h3>
                <form action="%site_address%apanel/update-prices" name="rabitsa" id="form-rabitsa" class="apanel-form">
                    <table class="site-table">
                        <thead>
                            <tr>
                                <td>Высота забора</td>
                                <td>Стоимость за метр</td>
                            </tr>
                        </thead>
                        <tbody>
                            %pricesRabitsa%
                        </tbody>
                    </table>
                    <div class="form-field flex button-right">
                        <div class="skew-right bg-y shadow button button-small upper apanel-submit">
                            Сохранить
                        </div>
                    </div>
                </form>
            </div>
            <div>
                <h3>Цены на сетку Гиттер</h3>
                <h4>С покрытием из ПВХ</h4>
                <form action="%site_address%apanel/update-prices" name="gitter_pvc" id="form-gitter-pvc" class="apanel-form">
                    <table class="site-table">
                        <thead>
                            <tr>
                                <td rowspan="2">Высота</td>
                                <td colspan="3">Толщина сетки</td>
                            </tr>
                            <tr>
                                <td>3 мм</td>
                                <td>4 мм</td>
                                <td>5 мм</td>
                            </tr>
                        </thead>
                        <tbody>
                            %pricesGitterPvc%
                        </tbody>
                    </table>
                    <div class="form-field flex button-right">
                        <div class="skew-right bg-y shadow button button-small upper apanel-submit">
                            Сохранить
                        </div>
                    </div>
                </form>
                <h4>Оцинкованную</h4>
                <form action="%site_address%apanel/update-prices" name="gitter_zinc" id="form-gitter-zinc" class="apanel-form">
                    <table class="site-table">
                        <thead>
                            <tr>
                                <td rowspan="2">Высота</td>
                                <td colspan="3">Толщина сетки</td>
                            </tr>
                            <tr>
                                <td>3 мм</td>
                                <td>3,5 мм</td>
                                <td>5 мм</td>
                            </tr>
                        </thead>
                        <tbody>
                            %pricesGitterZinc%
                        </tbody>
                    </table>
                    <div class="form-field flex button-right">
                        <div class="skew-right bg-y shadow button button-small upper apanel-submit">
                            Сохранить
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</main>
