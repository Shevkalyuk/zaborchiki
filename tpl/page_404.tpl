<main class="flex flex-column single-section">
    <section class="flex section-first">
        <div class="section-block bg-w center">
            <h1>Такой страницы у нас нет</h1>
            <p>Проверьте правильность ввода адреса.</p>
        </div
    </section>
</main>
