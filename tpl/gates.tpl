<main class="flex flex-column">
    <section class="flex section-vh fs-lh-3 h-margin">
        <div class="section-block bg-w">
            <ul class="section-block-tabs flex">
                <li class="section-tab__control active">
                    <a href="" class="section-tab__control-link">Подвесные</a>
                </li>
                <li class="section-tab__control">
                    <a href="" class="section-tab__control-link">Откатные и сдвижные</a>
                </li>
                <li class="section-tab__control">
                    <a href="" class="section-tab__control-link">Распашные</a>
                </li>
                <li class="section-tab__control">
                    <a href="" class="section-tab__control-link">Сдвижные с калиткой</a>
                </li>
            </ul>
            <div class="section-block-content flex flex-column">
                <div class="section-tab__content-item active gate-item">
                    <div class="gate-img">
                        <img src="%site_address%img/gates/hanging.jpg" alt="Подвесные ворота">
                    </div>
                    <div class="gate-desc flex flex-column text-block-right">
                        <h2>Подвесные ворота</h2>
                        <p>
                            Традиционно используются на промышленных объектах, отличаются повышенной прочностью
                            и надежностью. В основе конструкции лежит направляющая балка, расположнная вверху,
                            к которой крепится само полотно. Система не препятствует движению крупногабаритного
                            транспорта, ею легко управлять как вручную, так и с помощью автоматики.
                        </p>
                    </div>
                </div>
                <div class="section-tab__content-item gate-item">
                    <div class="gate-img">
                        <img src="%site_address%img/gates/console.jpg" alt="Откатные и сдвижные ворота">
                    </div>
                    <div class="gate-desc flex flex-column">
                        <h2>Откатные и сдвижные ворота</h2>
                        <p>
                            В основе - несущая балка, которая фиксирует низ полотна и обеспечивает его движение
                            с помощью специальных тележек, закрепленных в фундаменте.
                            Такие ворота легкие, прочные и способны выдержать большие нагрузки.
                        </p>
                    </div>
                </div>
                <div class="section-tab__content-item gate-item">
                    <div class="gate-img">
                        <img src="%site_address%img/gates/swing.jpg" alt="Распашные ворота">
                    </div>
                    <div class="gate-desc flex flex-column">
                        <h2>Распашные ворота</h2>
                        <p>
                            Распашные ворота – это простое и практичное решение для дачи,
                            где есть свободное пространство перед входом.
                        </p>
                    </div>
                </div>
                <div class="section-tab__content-item gate-item">
                    <div class="gate-img">
                        <img src="%site_address%img/gates/sliding_with_door.jpg" alt="Сдвижные ворота с калиткой">
                    </div>
                    <div class="gate-desc flex flex-column">
                        <h2>Сдвижные ворота с калиткой</h2>
                        <p>
                            А вот для домашних ограждений можно выбрать сдвижные ворота с калиткой.
                            Это очень удобно, так как каждый раз для того, чтобы попасть во двор,
                            человеку не понадобится открывать ворота полностью.
                        </p>
                    </div>
                </div>
            </div>
            <div class="section-block-buttons flex fs-2r">
                <div class="button bg-y skew-right fcw shadow htext65 center" data-modal="modal-gates-prices">
                    <div class="skew-left">
                        <span class="upper">Цена</span>
                    </div>
                </div>
                <div class="button bg-y skew-right fcw shadow htext65 center" data-modal="modal-order">
                    <div class="skew-left">
                        <span class="upper">Заказать</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
