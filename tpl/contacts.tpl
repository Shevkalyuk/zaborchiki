<main class="flex flex-column">
    <section class="flex section-vh fs-lh-3">
        <div class="section-block bg-w flex col-2">
            <div class="flex flex-column space-between h-100">
                <form class="feedback-form flex flex-column space-between h-100" action="#" method="post" id="feedback-message-form">
                    <div class="flex form-field-row col-2">
                        <div class="form-field">
                            <label for="input-name">Имя</label>
                            <input type="text" name="name" id="input-name">
                        </div>
                        <div class="form-field">
                            <label for="input-phone">Телефон</label>
                            <input type="text" name="phone" id="input-phone">
                        </div>
                    </div>
                    <div class="form-field">
                        <label for="input-email">e-mail</label>
                        <input type="email" name="email" id="input-email">
                    </div>
                    <div class="form-field">
                        <label for="input-message">Сообщение</label>
                        <textarea name="message" rows="6" cols="80" id="input-message"></textarea>
                    </div>
                    <div class="form-field flex button-right">
                        <div class="skew-right bg-y shadow button button-small upper" id="submit-message-form">
                            Отправить
                        </div>
                    </div>
                </form>
            </div>
            <div class="flex flex-column space-between h-100 contact-box-wrapper">
                <div class="contact-box bg-y flex">
                    <div class="contact-box-icon icon-email"></div>
                    <div class="contact-box-text">
                        vorotazab@yandex.ru
                    </div>
                </div>
                <div class="contact-box bg-y flex">
                    <div class="contact-box-icon icon-phone"></div>
                    <div class="contact-box-text">
                        <span>+7 (952) 392-75-50</span><br>
                        <span>+7 (952) 392-35-50</span>
                    </div>
                </div>
                <div class="contact-box bg-y flex">
                    <a href="%vk_link%" target="_blank" class="block flex">
                        <div class="contact-box-icon icon-vk"></div>
                        <div class="contact-box-text">
                            Наша группа ВКонтакте
                        </div>
                    </a>
                </div>
                <div class="right contact-box contact-box-wrapper-caption">
                    Заполните форму обратной связи и мы <br> Вам обязательно перезвоним!
                </div>
            </div>
        </div>
    </section>
</main>
