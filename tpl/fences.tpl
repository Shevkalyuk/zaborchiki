<main class="flex flex-column">
    <section class="flex section-first section-vh">
        <div class="section-block bg-w">
            <div class="section-block-caption flex">
                <div class="section-block-caption-wrapper">
                    <div class="bg-y skew-right center section-caption-bg">
                        <div class="skew-left">
                            <h1>Заборы для дачи: цена с установкой на основные типы конструкций</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-block-content flex">
                <div class="fence-item flex">
                    <div class="header cetner">
                        Забор из профлиста
                    </div>
                    <div class="image img-circle img-profnastil"></div>
                    <div class="skew-right bg-y shadow button button-small button-anchor">
                        <div class="skew-left">
                            <a href="#fence-profnastil" class="block">подробнее</a>
                        </div>
                    </div>
                </div>
                <div class="fence-item flex">
                    <div class="header cetner">
                        Металлический штакетник
                    </div>
                    <div class="image img-circle img-shtaketnik"></div>
                    <div class="skew-right bg-y shadow button button-small button-anchor">
                        <div class="skew-left">
                            <a href="#fence-shtaketnik" class="block">подробнее</a>
                        </div>
                    </div>
                </div>
                <div class="fence-item flex">
                    <div class="header cetner">
                        Сетка-рабица
                    </div>
                    <div class="image img-circle img-rabitsa"></div>
                    <div class="skew-right bg-y shadow button button-small button-anchor">
                        <div class="skew-left">
                            <a href="#fence-rabitsa" class="block">подробнее</a>
                        </div>
                    </div>
                </div>
                <div class="fence-item flex">
                    <div class="header cetner">
                        Сварная сетка Гиттер
                    </div>
                    <div class="image img-circle img-gitter"></div>
                    <div class="skew-right bg-y shadow button button-small button-anchor">
                        <div class="skew-left">
                            <a href="#fence-gitter" class="block">подробнее</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="flex section-vh" id="fence-profnastil">
        <div class="section-block bg-w">
            <div class="section-block-caption flex">
                <div class="section-block-caption-wrapper">
                    <div class="bg-y skew-right center section-caption-bg">
                        <div class="skew-left">
                            <h2>Забор из профлиста</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-block-content flex">
                <div class="fence-schema">
                    <img src="%site_address%img/fence_sch/profnastil.png" alt="Схема забора из профнастила">
                </div>
                <div class="fence-desc">
                    <p>
                        Забор из профлиста характеризует долговечность, невысокая стоимость
                        (в сравнении с иными стройматериалами), огнестойкость, прочность, простой и быстрый монтаж,
                        хорошая ветро- и шумозащита, способность хорошо сочетаться с другими материалами.
                    </p>
                    <p>
                        Покрытие поверхности профилированного листа специальными
                        полимерами позволяет увеличить срок службы до 50–55 лет.
                    </p>
                </div>
            </div>
            <div class="section-block-buttons flex fs-2r">
                <div class="button bg-y skew-right fcw shadow htext65 center" data-modal="modal-profnastil-prices">
                    <div class="skew-left">
                        <span class="upper">Цена</span>
                    </div>
                </div>
                <div class="button bg-y skew-right fcw shadow htext65 center" data-modal="modal-order">
                    <div class="skew-left">
                        <span class="upper">Заказать</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="flex section-vh" id="fence-shtaketnik">
        <div class="section-block bg-w">
            <div class="section-block-caption flex">
                <div class="section-block-caption-wrapper">
                    <div class="bg-y skew-right center section-caption-bg">
                        <div class="skew-left">
                            <h2>Металлический штакетник</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-block-content flex">
                <div class="fence-schema">
                    <img src="%site_address%img/fence_sch/shtaketnik.png" alt="Схема металлического штакетника">
                </div>
                <div class="fence-desc">
                    <p>
                        Штакетник производится из оцинкованного металла, толщиной около 0,5 см.
                        Высота штакетин от 0,5 до 3,0 м. Покрывается полимерными веществами, гарантирующими
                        защиту от мелких механических повреждений (потертостей, царапин и т.п.) и коррозии.
                    </p>
                    <p>
                        Дизайнеры рекомендуют сочетать металлический штакетник с другими материалами
                        – кирпичом, камнем и т.п.
                    </p>
                </div>
            </div>
            <div class="section-block-buttons flex fs-2r">
                <div class="button bg-y skew-right fcw shadow htext65 center" data-modal="modal-shtaketnik-prices">
                    <div class="skew-left">
                        <span class="upper">Цена</span>
                    </div>
                </div>
                <div class="button bg-y skew-right fcw shadow htext65 center" data-modal="modal-order">
                    <div class="skew-left">
                        <span class="upper">Заказать</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="flex section-vh" id="fence-rabitsa">
        <div class="section-block bg-w">
            <div class="section-block-caption flex">
                <div class="section-block-caption-wrapper">
                    <div class="bg-y skew-right center section-caption-bg">
                        <div class="skew-left">
                            <h2>Сетка-рабица</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-block-content flex">
                <div class="fence-schema">
                    <img src="%site_address%img/fence_sch/rabitsa.png" alt="Схема сетки-рабицы">
                </div>
                <div class="fence-desc">
                    <p>
                        Конструкция:
                    </p>
                    <ul class="marked">
                        <li>Столбы диаметром 51 мм</li>
                        <li>Сетка-рабица 50х50 с покрытием ПВХ</li>
                        <li>Лага 1 шт.</li>
                        <li>Крепление через ленту, крашенную в цвет сетки</li>
                    </ul>
                    <p>
                        Стоимость ворот и калитки под ключ:
                    </p>
                    <ul class="marked">
                        <li>Калитка: сетка-рабица ПВХ. Стоимость 4500 р.</li>
                        <li>Ворота: сетка-рабица ПВХ. Стоимость 12000 р.</li>
                    </ul>
                    <p>
                        Дополнения к забору:
                    </p>
                    <ul class="marked">
                        <li>Забор на винтовых сваях +900 р. за м/п</li>
                        <li>Ворота на винтовых сваях +5000 р.</li>
                        <li>Калитка на винтовых сваях +2500 р.</li>
                        <li>Доп. лага +150 р./м</li>
                        <li>Установка замка от 500 р.</li>
                        <li>Демонтаж забора от 100 р./м</li>
                        <li>Бетонирование от 150 р./столб</li>
                        <li>Колючая защита по согласованию</li>
                    </ul>
                </div>
            </div>
            <div class="section-block-buttons flex fs-2r">
                <div class="button bg-y skew-right fcw shadow htext65 center" data-modal="modal-rabitsa-prices">
                    <div class="skew-left">
                        <span class="upper">Цена</span>
                    </div>
                </div>
                <div class="button bg-y skew-right fcw shadow htext65 center" data-modal="modal-order">
                    <div class="skew-left">
                        <span class="upper">Заказать</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="flex section-vh" id="fence-gitter">
        <div class="section-block bg-w">
            <div class="section-block-caption flex">
                <div class="section-block-caption-wrapper">
                    <div class="bg-y skew-right center section-caption-bg">
                        <div class="skew-left">
                            <h2>Сварная сетка Гиттер</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-block-content flex">
                <div class="fence-schema">
                    <img src="%site_address%img/fence_sch/gitter.png" alt="Схема сварной сетки Гиттер">
                </div>
                <div class="fence-desc">
                    <p>
                        Сетка сварная Гиттер оцинкованная с цветным полимерным покрытием или без покрытия.
                        Столбы 50х50 мм, метод установки «бутование щебнем».
                    </p>
                    <p>
                        Стоимость указана с учетом установки и материалов за погонный метр.
                    </p>
                </div>
            </div>
            <div class="section-block-buttons flex fs-2r">
                <div class="button bg-y skew-right fcw shadow htext65 center" data-modal="modal-gitter-prices">
                    <div class="skew-left">
                        <span class="upper">Цена</span>
                    </div>
                </div>
                <div class="button bg-y skew-right fcw shadow htext65 center" data-modal="modal-order">
                    <div class="skew-left">
                        <span class="upper">Заказать</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
