-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 21 2018 г., 09:07
-- Версия сервера: 5.7.20
-- Версия PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `zaborvorota`
--

-- --------------------------------------------------------

--
-- Структура таблицы `pricesGitterPvc`
--

CREATE TABLE `pricesGitterPvc` (
  `id` int(11) UNSIGNED NOT NULL,
  `height` float(4,2) NOT NULL,
  `thickness1` decimal(10,2) DEFAULT NULL,
  `thickness2` decimal(10,2) DEFAULT NULL,
  `thickness3` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pricesGitterPvc`
--

INSERT INTO `pricesGitterPvc` (`id`, `height`, `thickness1`, `thickness2`, `thickness3`) VALUES
(1, 1.50, '1100.00', '1250.00', NULL),
(2, 2.00, '1200.00', '1400.00', '1800.00');

-- --------------------------------------------------------

--
-- Структура таблицы `pricesGitterZinc`
--

CREATE TABLE `pricesGitterZinc` (
  `id` int(11) UNSIGNED NOT NULL,
  `height` float(4,2) NOT NULL,
  `thickness1` decimal(10,2) DEFAULT NULL,
  `thickness2` decimal(10,2) DEFAULT NULL,
  `thickness3` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pricesGitterZinc`
--

INSERT INTO `pricesGitterZinc` (`id`, `height`, `thickness1`, `thickness2`, `thickness3`) VALUES
(1, 1.50, '1050.00', '1150.00', NULL),
(2, 2.00, '1100.00', '1250.00', '1650.00');

-- --------------------------------------------------------

--
-- Структура таблицы `pricesMetalFence`
--

CREATE TABLE `pricesMetalFence` (
  `id` int(11) UNSIGNED NOT NULL,
  `height` float(4,2) NOT NULL,
  `logs` int(10) UNSIGNED NOT NULL,
  `unilaterally` decimal(10,2) NOT NULL,
  `stagged` decimal(10,2) NOT NULL,
  `ribbonFoundation` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pricesMetalFence`
--

INSERT INTO `pricesMetalFence` (`id`, `height`, `logs`, `unilaterally`, `stagged`, `ribbonFoundation`) VALUES
(1, 1.50, 2, '2000.00', '2800.00', '3000.00'),
(2, 1.80, 2, '2050.00', '2850.00', '3000.00'),
(3, 2.00, 2, '2100.00', '2900.00', '3000.00');

-- --------------------------------------------------------

--
-- Структура таблицы `pricesProfnastil`
--

CREATE TABLE `pricesProfnastil` (
  `id` int(11) NOT NULL,
  `height` float(4,2) NOT NULL,
  `oneSide` decimal(10,2) NOT NULL,
  `twoSides` decimal(10,2) NOT NULL,
  `zink` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pricesProfnastil`
--

INSERT INTO `pricesProfnastil` (`id`, `height`, `oneSide`, `twoSides`, `zink`) VALUES
(1, 1.50, '1500.00', '1700.00', '1300.00'),
(2, 1.80, '1550.00', '1750.00', '1350.00'),
(3, 2.00, '1600.00', '1800.00', '1400.00');

-- --------------------------------------------------------

--
-- Структура таблицы `pricesRabitsa`
--

CREATE TABLE `pricesRabitsa` (
  `id` int(11) UNSIGNED NOT NULL,
  `height` float(4,2) NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pricesRabitsa`
--

INSERT INTO `pricesRabitsa` (`id`, `height`, `price`) VALUES
(1, 1.50, '800.00'),
(2, 2.00, '850.00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `pricesGitterPvc`
--
ALTER TABLE `pricesGitterPvc`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pricesGitterZinc`
--
ALTER TABLE `pricesGitterZinc`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pricesMetalFence`
--
ALTER TABLE `pricesMetalFence`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pricesProfnastil`
--
ALTER TABLE `pricesProfnastil`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pricesRabitsa`
--
ALTER TABLE `pricesRabitsa`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `pricesGitterPvc`
--
ALTER TABLE `pricesGitterPvc`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `pricesGitterZinc`
--
ALTER TABLE `pricesGitterZinc`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `pricesMetalFence`
--
ALTER TABLE `pricesMetalFence`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `pricesProfnastil`
--
ALTER TABLE `pricesProfnastil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `pricesRabitsa`
--
ALTER TABLE `pricesRabitsa`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
