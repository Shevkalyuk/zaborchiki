<?php

require_once 'ApanelContent.php';
require_once 'lib/NotFoundContent.php';

class HTTPAuth
{
    private $route = '';
    private $action = null;

    private $user = 'admin';
    private $pwd = '8zkMaf2BLt';

    public function __construct($route, $action)
    {
        $this->route = $route;
        $this->action = $action;
    }

    public function login()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])
            || !isset($_SERVER['PHP_AUTH_PW'])
            || $_SERVER['PHP_AUTH_USER'] !== $this->user
            || $_SERVER['PHP_AUTH_PW'] !== $this->pwd) {
            header('WWW-Authenticate: Basic realm="My Realm"');
            header('HTTP/1.0 401 Unauthorized');
            echo 'Unauthorized access forbidden';
            exit;
        } else {
            switch ($this->route) {
                case 'apanel':
                    $content = new ApanelContent();
                    break;
                default:
                    $content = new NotFoundContent();
                    break;
            }

            if (!is_null($this->action) && method_exists($content, 'action' . $this->action)) {
                $action = 'action' . $this->action;
                echo $content->$action();
            } else {
                echo $content->getContent();
            }
        }
    }
}
