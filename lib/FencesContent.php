<?php

class FencesContent extends Modules
{
    private $tableNames = [
        'pricesProfnastil' => 'table-row-prices-profnastil',
        'pricesMetalFence' => 'table-row-prices-metal-fence',
        'pricesRabitsa' => 'table-row-prices-rabitsa',
        'pricesGitterPvc' => 'table-row-prices-gitter',
        'pricesGitterZinc' => 'table-row-prices-gitter',
    ];

    /**
     * @return mixed
     */
    protected function getTitle()
    {
        return 'NK-Групп - Заборы';
    }

    /**
     * @return mixed
     */
    protected function getDescription()
    {
        return 'Производство и установка откатных и распашных ворот в Санкт-Петербурге и Ленинградской области.';
    }

    /**
     * @return mixed
     */
    protected function getMainContent()
    {
        return $this->getTemplate('fences');
    }

    protected function getModals()
    {
        $priceData = [];

        foreach ($this->tableNames as $tableName => $tplName) {
            $priceData[$tableName] = $this->fillPriceTemplate($tableName, $tplName);
        }

        return parent::getModals() . $this->getReplaceTemplate($priceData, 'modals-fences');
    }

    private function fillPriceTemplate($tableName, $tplName)
    {
        $result = '';
        $lines = $this->db->getAll($tableName);

        foreach ($lines as $line) {
            foreach ($line as $field => &$data) {
                // Округление цен, если не будет копеек
                if (is_numeric($data) && (int)$data - $data === 0.00) {
                    $data = (int)$data;
                }

                if (strpos($field, 'thickness') === 0) {
                    $data = empty($data) ? '-' : $data . ' р.';
                }
            }

            $result .= $this->getReplaceTemplate($line, $tplName);
        }

        return $result;
    }
}
