<?php

require_once 'MyLogger.php';

class DatabasePDO
{
    private static $instance;

    private $dbHost = 'localhost';
    private $dbName;
    private $dbUser;
    private $dbPassword;

    private $pdo;

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new self(Config::getDbName(), Config::getDbUser(), Config::getDbPwd());
        }

        return self::$instance;
    }

    private function __construct($dbName, $dbUser, $dbPwd)
    {
        $this->dbName = $dbName;
        $this->dbUser = $dbUser;
        $this->dbPassword = $dbPwd;

        $dsn = 'mysql:host=' . $this->dbHost . ';dbname=' . $this->dbName;

        try {
            $this->pdo = new PDO($dsn, $this->dbUser, $this->dbPassword);
        }  catch (PDOException $e) {
            MyLogger::lg($e->getMessage(), 'DATABASE ERROR');
        }
    }

    public function getAll($tableName)
    {
        $sql = 'SELECT * FROM ' . $tableName;
        $st = $this->pdo->prepare($sql);
        $st->execute();

        return $st->fetchAll(PDO::FETCH_ASSOC);
    }

    public function updateById($tableName, $id, array $values)
    {
        $sql = 'UPDATE ' . htmlspecialchars($tableName) . ' SET ';

        foreach ($values as $field => $value) {
            $sql .= htmlspecialchars($field) . ' = ' . htmlspecialchars($value) . ', ';
        }

        $sql = substr($sql, 0, -2);
        $sql .= ' WHERE id = ' . $id;

        MyLogger::lg($sql, 'DATABASE QUERY');

        $st = $this->pdo->prepare($sql);
        return $st->execute();
    }
}
