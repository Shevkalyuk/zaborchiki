<?php

require_once 'lib/MyLogger.php';

class ApanelContent extends Modules
{
    private $tableNames = [
        'pricesProfnastil' => 'table-row-prices-profnastil-form',
        'pricesMetalFence' => 'table-row-prices-metal-fence-form',
        'pricesRabitsa' => 'table-row-prices-rabitsa-form',
        'pricesGitterPvc' => 'table-row-prices-gitter-form',
        'pricesGitterZinc' => 'table-row-prices-gitter-form',
    ];

    public function actionUpdatePrices()
    {
        $method = $_SERVER['REQUEST_METHOD'];

        if ($method === 'POST') {
            try {
                $data = $_POST;
                $result = [];

                foreach ($data as $rowKey => $rowData) {
                    $rowKeyParts = explode('_', $rowKey);

                    foreach ($rowData as $filed => &$value) {
                        if (empty($value)) {
                            $value = 'NULL';
                        }
                    }

                    $result[] = $this->db->updateById($rowKeyParts[0], $rowKeyParts[1], $rowData);
                }

                return reset(array_unique($result)) === true;
            } catch (Exception $e) {
                MyLogger::lg($e->getMessage(), 'Update data error');
                header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
                exit;
            }
        } else {
            header($_SERVER['SERVER_PROTOCOL'] . ' 405 Method Not Allowed', true, 405);
            exit;
        }
    }

    protected function getPageMeta()
    {
        return '<meta name="robots" content="noindex">';
    }

    /**
     * @return mixed
     */
    protected function getTitle()
    {
        return 'Заборчики - панель  администратора';
    }

    /**
     * @return mixed
     */
    protected function getDescription()
    {
        return 'Сайт компании NK - панель администратора';
    }

    /**
     * @return mixed
     */
    protected function getMainContent()
    {
        $priceData = [];

        foreach ($this->tableNames as $tableName => $tplName) {
            $priceData[$tableName] = $this->fillPriceTemplate($tableName, $tplName);
        }

        return $this->getReplaceTemplate($priceData, 'apanel');
    }

    protected function getFooter()
    {
        return '';
    }

    protected function getHeader()
    {
        return $this->getTemplate('header_admin');
    }

    protected function getModals()
    {
        return parent::getModals() . $this->getTemplate('modals-apanel');
    }

    private function fillPriceTemplate($tableName, $tplName)
    {
        $result = '';
        $lines = $this->db->getAll($tableName);

        foreach ($lines as $line) {
            foreach ($line as $field => &$data) {
                // Округление цен, если не будет копеек
                if (is_numeric($data) && (int)$data - $data === 0.00) {
                    $data = (int)$data;
                }
            }

            $replaceData = array_merge($line, ['tableName' => $tableName]);
            $result .= $this->getReplaceTemplate($replaceData, $tplName);
        }

        return $result;
    }
}
