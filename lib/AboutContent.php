<?php

class AboutContent extends Modules
{
    /**
     * @return mixed
     */
    protected function getTitle()
    {
        return 'NK-Групп - Заборы';
    }

    /**
     * @return mixed
     */
    protected function getDescription()
    {
        return 'Производство и установка откатных и распашных ворот в Санкт-Петербурге и Ленинградской области.';
    }

    /**
     * @return mixed
     */
    protected function getMainContent()
    {
        return $this->getTemplate('about');
    }
}
