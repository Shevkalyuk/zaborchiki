<?php

class GatesContent extends Modules
{

    /**
     * @return mixed
     */
    protected function getTitle()
    {
        return 'Заборчики - ворота и калитки';
    }

    /**
     * @return mixed
     */
    protected function getDescription()
    {
        return 'Производство и установка откатных и распашных ворот в Санкт-Петербурге и Ленинградской области.';
    }

    /**
     * @return mixed
     */
    protected function getMainContent()
    {
        return $this->getTemplate('gates');
    }

    protected function getModals()
    {
        return parent::getModals() . $this->getTemplate('modals-gates');
    }
}
