<?php

class BlogContent extends Modules
{

    /**
     * @return mixed
     */
    protected function getTitle()
    {
        return 'Заборчики - ворота и калитки';
    }

    /**
     * @return mixed
     */
    protected function getDescription()
    {
        return 'Блог компании NK. Производство и установка откатных и распашных ворот в Санкт-Петербурге и Ленинградской области.';
    }

    /**
     * @return mixed
     */
    protected function getMainContent()
    {
        return $this->getTemplate('maintenance');
    }
}
