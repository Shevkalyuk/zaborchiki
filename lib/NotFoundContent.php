<?php

require_once 'Modules.php';

/**
 * Class NotFoundContent
 */
class NotFoundContent extends Modules
{
    /**
     * NotFoundContent constructor.
     */
    public function __construct()
    {
        parent::__construct();
        header('HTTP/1.1 404 Not Found');
    }

    /**
     * @return string
     */
    protected function getTitle()
    {
        return 'Страница не найдена';
    }

    /**
     * @return string
     */
    protected function getDescription()
    {
        return 'Запрошенная страница не существует.';
    }

    /**
     * @return mixed
     */
    protected function getMainContent()
    {
        return $this->getTemplate('page_404');
    }

    /**
     * @return string
     */
    protected function getBottom()
    {
        return '';
    }
}
