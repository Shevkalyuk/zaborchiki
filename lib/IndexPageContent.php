<?php

require_once 'Modules.php';

/**
 * Class IndexPageContent
 */
class IndexPageContent extends Modules
{
    /**
     * IndexPageContent constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return string
     */
    protected function getTitle()
    {
        return 'NK-Групп';
    }

    /**
     * @return string
     */
    protected function getBodyClass()
    {
        return 'b-index';
    }

    protected function getHeader()
    {
        return '';
    }

    /**
     * @return string
     */
    protected function getDescription()
    {
        return 'Производство и установка откатных и распашных ворот в Санкт-Петербурге и Ленинградской области.';
    }

    /**
     * @return mixed
     */
    protected function getMainContent()
    {
        return $this->getTemplate('index');
    }

    protected function getFooter()
    {
        return '';
    }
}
