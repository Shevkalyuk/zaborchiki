<?php

class Config
{
    const ENV_DEV = 'dev';
    const ENV_TEST = 'test';
    const ENV_PROD = 'prod';

    private static $environment = self::ENV_DEV;

    public $projectName = 'Заборчики';

    public $siteEmail = 'no-reply@madatsci.ru';
    public $emailSubjectFeedback = 'Новое сообщение от пользователя на сайте ';
    public $emailSubjectOrder = 'Новый заказ на сайте ';
    public $adminEmails = [
        'm-dec@yandex.ru,',
        'vorotazab@yandex.ru',
    ];

    public $sitename = 'zaborvorota.spb.ru';
    public $tplDir = 'tpl/';
    public $tplExt = '.tpl';

    public $vkLink = 'https://vk.com/zaborvorotaspb';

    /**
     * @return string
     */
    public static function baseUrl()
    {
        switch (self::$environment) {
            case self::ENV_DEV:
                $url = 'http://zaborchiki.local/';
                break;
            case self::ENV_TEST:
                $url = 'http://madatsci.ru/dev/zaborchiki/';
                break;
            case self::ENV_PROD:
            default:
                $url = 'http://zaborvorota.spb.ru/';
                break;
        }

        return $url;
    }

    public static function getLogName()
    {
        switch (self::$environment) {
            case self::ENV_DEV:
                $logName = 'dev';
                break;
            case self::ENV_TEST:
                $logName = 'test';
                break;
            case self::ENV_PROD:
            default:
                $logName = 'prod';
                break;
        }

        return $logName;
    }

    public static function getDbName()
    {
        switch (self::$environment) {
            case self::ENV_DEV:
                $dbName = 'zaborvorota';
                break;
            case self::ENV_TEST:
                $dbName = 'zaborvorota';
                break;
            case self::ENV_PROD:
            default:
                $dbName = 'madatsci_zaborvorota';
                break;
        }

        return $dbName;
    }

    public static function getDbUser()
    {
        switch (self::$environment) {
            case self::ENV_DEV:
                $dbUser = 'root';
                break;
            case self::ENV_TEST:
                $dbUser = 'madatsci';
                break;
            case self::ENV_PROD:
            default:
                $dbUser = '046522370_zv';
                break;
        }

        return $dbUser;
    }

    public static function getDbPwd()
    {
        switch (self::$environment) {
            case self::ENV_DEV:
                $dbPwd = '';
                break;
            case self::ENV_TEST:
                $dbPwd = '';
                break;
            case self::ENV_PROD:
            default:
                $dbPwd = 'MavJDXY{2D;L';
                break;
        }

        return $dbPwd;
    }

    /**
     * @return string
     */
    public static function getUriSubstrLength()
    {
        return self::isTest() ? 16 : 1;
    }

    /**
     * @return bool
     */
    public static function isDev()
    {
        return self::$environment === self::ENV_DEV;
    }

    public static function isTest()
    {
        return self::$environment === self::ENV_TEST;
    }

    public static function isProd()
    {
        return self::$environment === self::ENV_PROD;
    }
}
