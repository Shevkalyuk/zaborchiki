<?php

require_once 'Config.php';
require_once 'DatabasePDO.php';

/**
 * Class Modules
 */
abstract class Modules
{
    /**
     * @var Config
     */
    protected $config;

    protected $db;

    /**
     * Modules constructor.
     */
    public function __construct()
    {
        $this->config = new Config();
        $this->db = DatabasePDO::getInstance();
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        $params['page_meta'] = $this->getPageMeta();
        $params['title'] = $this->getTitle();
        $params['meta_desc'] = $this->getDescription();
        $params['body_class'] = $this->getBodyClass();
        $params['header'] = $this->getHeader();
        $params['main_content'] = $this->getMainContent();
        $params['footer'] = $this->getFooter();
        $params['modals'] = $this->getModals();

        return $this->getReplaceTemplate($params, 'layout');
    }

    protected function getBodyClass()
    {
        return 'b-regular';
    }

    protected function getPageMeta()
    {
        return '';
    }

    /**
     * @return mixed
     */
    abstract protected function getTitle();

    /**
     * @return mixed
     */
    abstract protected function getDescription();

    /**
     * @return mixed
     */
    abstract protected function getMainContent();

    /**
     * @return string
     */
    protected function getFooter()
    {
        return $this->getTemplate('footer');
    }

    /**
     * @return string
     */
    protected function getModals()
    {
        return $this->getTemplate('modal-order');
    }

    /**
     * @return mixed
     */
    protected function getHeader()
    {
        return $this->getTemplate('header');
    }

    /**
     * @param string $name
     * @return mixed
     */
    protected function getTemplate($name)
    {
        $textContent = file_get_contents($this->config->tplDir . $name . $this->config->tplExt);
        $search = ['%site_address%', '%vk_link%'];
        $replace = [Config::baseUrl(), $this->config->vkLink];

        return str_replace($search, $replace, $textContent);
    }

    /**
     * @param array $params
     * @param string $templateName
     * @return mixed
     */
    protected function getReplaceTemplate($params, $templateName)
    {
        return $this->getReplaceContent($params, $this->getTemplate($templateName));
    }

    /**
     * @param array $params
     * @param string $content
     * @return mixed
     */
    private function getReplaceContent($params, $content)
    {
        $search = [];
        $replace = [];
        $i = 0;

        foreach ($params as $key => $value) {
            $search[$i] = '%' . $key . '%';
            $replace[$i] = $value;
            $i++;
        }

        return str_replace($search, $replace, $content);
    }
}
