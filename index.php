<?php

mb_internal_encoding("UTF-8");

require_once 'lib/Config.php';
require_once 'lib/MyLogger.php';
require_once 'lib/IndexPageContent.php';
require_once 'lib/GatesContent.php';
require_once 'lib/FencesContent.php';
require_once 'lib/AboutContent.php';
require_once 'lib/BlogContent.php';
require_once 'lib/ContactsContent.php';
require_once 'lib/NotFoundContent.php';
require_once 'lib/HTTPAuth.php';

$uri = substr($_SERVER["REQUEST_URI"], Config::getUriSubstrLength());
MyLogger::lg($_SERVER["REQUEST_METHOD"] . ' ' . $_SERVER["REQUEST_URI"], 'REQUEST');
$action = null;

if (strpos($uri, 'apanel/') !== false) {
    $uriParts = explode('/', $uri);
    $uri = $uriParts[0];
    $action = $uriParts[1];

    if (strpos($action, '-') !== false) {
        $actionParts = explode('-', $action);
        $action = '';

        foreach ($actionParts as $word) {
            $action .= ucfirst($word);
        }
    }
}

switch ($uri) {
    case '':
        $content = new IndexPageContent();
        break;
    case 'gates':
        $content = new GatesContent();
        break;
    case 'fences':
        $content = new FencesContent();
        break;
    case 'about':
        $content = new AboutContent();
        break;
    case 'blog':
        $content = new BlogContent();
        break;
    case 'contacts':
        $content = new ContactsContent();
        break;
    case 'apanel':
        (new HTTPAuth('apanel', $action))->login();
        exit;
    default:
        $content = new NotFoundContent();
        break;
}

echo $content->getContent();
